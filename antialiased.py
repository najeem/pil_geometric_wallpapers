from PIL import Image, ImageDraw

final_image_size = (1920, 1080)

img = Image.new("RGB", (500, 500))
ic = ImageDraw.Draw(img)

ic.ellipse([0, 0, 500, 500], fill="RED")

img.save("basic.png")