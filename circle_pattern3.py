from PIL import Image, ImageDraw
import random
import math
from scipy.spatial import Delaunay
import requests
from bs4 import BeautifulSoup as bs

def get_top_pallet(id):
    pallets_xml = requests.get("http://www.colourlovers.com/api/palettes/top")
    pallets_parsed = bs(pallets_xml.content, 'html.parser')
    pallets = pallets_parsed.find_all('colors')
    colors = []
    rand_pallet = pallets[id]
    for color in rand_pallet.find_all('hex'):
        colors.append("#" + color.text)
    return colors

width, height = 16, 9
step = 500
image_width, image_height  = width*step, height*step

a = Image.new('RGB', (image_width, image_height))
d = ImageDraw.Draw(a, "RGBA")

def draw_circle(d, centerx, centery, radius, **kwargs):
    d.ellipse(
        [
            centerx-radius,
            centery-radius,
            centerx+radius,
            centery+radius
        ], **kwargs
    )

colors = get_top_pallet(18)

for i in range(width*10, 0, -1):
    draw_circle(d, 
                width*step/2,
                height*step/2,
                step*i/10, 
                fill=random.choice(colors)+"ff",
                )

a = a.resize((1920, 1080), resample=Image.ANTIALIAS)
a.save(f"{__file__}.png")