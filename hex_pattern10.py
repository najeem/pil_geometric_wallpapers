from PIL import Image, ImageDraw
import random
import math
import numpy as np
from scipy.spatial import Delaunay
from scipy.interpolate import interp2d
import requests
from bs4 import BeautifulSoup as bs

def random_pallet():
    pallets_xml = requests.get("http://www.colourlovers.com/api/palettes/random")
    pallets_parsed = bs(pallets_xml.content, 'html.parser')
    pallets = pallets_parsed.find_all('colors')
    colors = []
    rand_pallet = pallets[0]
    for color in rand_pallet.find_all('hex'):
        colors.append(color.text)
    return colors

def get_color(point):
    x, y = point
    x_ = [0, image_width, 0, image_width, image_width/2]
    y_ = [0, 0, image_height, image_height, image_height/2]
    spectra = np.array([
        [int(color[i:i+2], 16) for i in (0, 2, 4)] for color in colors[:5]
    ])
    z_r, z_g, z_b = spectra.T[0], spectra.T[1], spectra.T[2] 
    f_r = interp2d(x_, y_, z_r)
    f_g = interp2d(x_, y_, z_g)
    f_b = interp2d(x_, y_, z_b)
    color = np.concatenate((f_r(x, y), f_g(x, y), f_b(x, y)))
    color = np.clip(color, 0, 255)
    return color.astype(int)

width, height = 16, 9
step = 500
image_width, image_height  = width*step, height*step

a = Image.new('RGB', (image_width, image_height))
d = ImageDraw.Draw(a, "RGBA")

def draw_hexagon(d, centerx, centery, radius, **kwargs):
    d.polygon(
        [
            centerx-math.sin(math.pi/180*30)*radius, centery-math.cos(math.pi/180*30)*radius,
            centerx+math.sin(math.pi/180*30)*radius, centery-math.cos(math.pi/180*30)*radius,
            centerx+radius, centery,
            centerx+math.sin(math.pi/180*30)*radius, centery+math.cos(math.pi/180*30)*radius,
            centerx-math.sin(math.pi/180*30)*radius, centery+math.cos(math.pi/180*30)*radius,
            centerx-radius, centery,
        ], **kwargs
    )

colors = random_pallet()

for i in range(-1,width*2):
    for j in range(-1, height+4):
        color = "#{:02x}{:02x}{:02x}ff".format(*get_color((i*step, j*step)))
        draw_hexagon(
            d, 
            (i)*step/2*1.5,
            (j+i%2/2)*math.cos(math.pi/6)*step, step/2, 
            fill=color,
        )

a = a.resize((1920, 1080), resample=Image.ANTIALIAS)
a.save(f"{__file__}.png")