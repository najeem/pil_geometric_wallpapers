from PIL import Image, ImageDraw
import random
import math
from scipy.spatial import Delaunay
import requests
from bs4 import BeautifulSoup as bs

def get_top_pallet(id):
    pallets_xml = requests.get("http://www.colourlovers.com/api/palettes/top")
    pallets_parsed = bs(pallets_xml.content, 'html.parser')
    pallets = pallets_parsed.find_all('colors')
    colors = []
    rand_pallet = pallets[id]
    for color in rand_pallet.find_all('hex'):
        colors.append("#" + color.text)
    return colors

width, height = 16, 9
step = 500
image_width, image_height  = width*step, height*step

a = Image.new('RGB', (image_width, image_height))
d = ImageDraw.Draw(a, "RGBA")

def draw_hexagon(d, centerx, centery, radius, **kwargs):
    d.polygon(
        [
            centerx-math.sin(math.pi/180*30)*radius, centery-math.cos(math.pi/180*30)*radius,
            centerx+math.sin(math.pi/180*30)*radius, centery-math.cos(math.pi/180*30)*radius,
            centerx+radius, centery,
            centerx+math.sin(math.pi/180*30)*radius, centery+math.cos(math.pi/180*30)*radius,
            centerx-math.sin(math.pi/180*30)*radius, centery+math.cos(math.pi/180*30)*radius,
            centerx-radius, centery,
        ], **kwargs
    )

colors = get_top_pallet(11)

for i in range(-1,width+2):
    for j in range(-1, height+4):
        draw_hexagon(d, 
                    (i)*step, 
                    (j+i%2/2)*math.cos(math.pi/6)*step, step*1.5, 
                    fill=colors[(i+j)%len(colors)]+str(11*(i%2+1)),
                    outline="#ffffffff"
                   )

a = a.resize((1920, 1080), resample=Image.ANTIALIAS)
a.save(f"{__file__}.png")