from PIL import Image, ImageDraw
import numpy as np
from scipy.spatial import Delaunay
import requests
from bs4 import BeautifulSoup as bs

def random_pallet():
    pallets_xml = requests.get("http://www.colourlovers.com/api/palettes/top")
    pallets_parsed = bs(pallets_xml.content, 'html.parser')
    pallets = pallets_parsed.find_all('colors')
    colors = []
    rand_pallet = pallets[np.random.randint(len(pallets))]
    for color in rand_pallet.find_all('hex'):
        colors.append("#" + color.text)
    return colors

myres = np.array([16, 9])
imageres = myres*500

xpoints = np.random.randint(-500, imageres[0]+500, size=1000)
ypoints = np.random.randint(-500, imageres[1]+500, size=1000)
points = np.column_stack((xpoints, ypoints))

a = Image.new('RGB', tuple(imageres))
d = ImageDraw.Draw(a, "RGB")

from scipy.spatial import Delaunay
trias = Delaunay(points)
colors = random_pallet()
for tria in points[trias.simplices]:
    d.polygon(list(np.reshape(tria, (6,))), fill=colors[np.random.randint(5)])
a = a.resize((1920, 1080), resample=Image.ANTIALIAS)
a.save("random_trias.png")