import numpy as np
from scipy.interpolate import interp2d
from scipy.spatial import Delaunay

imageres = [1920, 1080]

def get_color(point):
    x, y = np.mean(point, axis=0)
    print(x, y)
    x_ = [0, imageres[0], 0, imageres[0]]
    y_ = [0, 0, imageres[1], imageres[1]]
    z_r = [255, 0, 0, 128]
    z_g = [0, 255, 0, 128]
    z_b = [0, 0, 255, 0]
    f_r = interp2d(x_, y_, z_r)
    f_g = interp2d(x_, y_, z_g)
    f_b = interp2d(x_, y_, z_b)
    return np.concatenate((f_r(x, y), f_g(x, y), f_b(x, y)))

xpoints = np.random.randint(0, imageres[0], size=10)
ypoints = np.random.randint(0, imageres[1], size=10)
points = np.column_stack((xpoints, ypoints))

from scipy.spatial import Delaunay
trias = Delaunay(points)
for tria in points[trias.simplices]:
    print(get_color(tria))