from PIL import Image, ImageDraw
import numpy as np
from scipy.spatial import Delaunay
from scipy.interpolate import interp2d
import requests
from bs4 import BeautifulSoup as bs

def random_pallet(id):
    pallets_xml = requests.get("http://www.colourlovers.com/api/palettes/random")
    pallets_parsed = bs(pallets_xml.content, 'html.parser')
    pallets = pallets_parsed.find_all('colors')
    colors = []
    rand_pallet = pallets[0]
    for color in rand_pallet.find_all('hex'):
        colors.append(color.text)
    return colors

myres = np.array([16, 9])
imageres = myres*500

def get_color(point):
    x, y = point
    x_ = [0, imageres[0], 0, imageres[0], imageres[0]/2]
    y_ = [0, 0, imageres[1], imageres[1], imageres[1]/2]
    spectra = np.array([
        [int(color[i:i+2], 16) for i in (0, 2, 4)] for color in colors[:5]
    ])
    z_r, z_g, z_b = spectra.T[0], spectra.T[1], spectra.T[2] 
    f_r = interp2d(x_, y_, z_r)
    f_g = interp2d(x_, y_, z_g)
    f_b = interp2d(x_, y_, z_b)
    color = np.concatenate((f_r(x, y), f_g(x, y), f_b(x, y)))
    return color.astype(int)

xpoints = np.random.randint(-1000, imageres[0]+1000, size=500)
ypoints = np.random.randint(-1000, imageres[1]+1000, size=500)
points = np.column_stack((xpoints, ypoints))

a = Image.new('RGB', tuple(imageres))
d = ImageDraw.Draw(a, "RGBA")

from scipy.spatial import Delaunay
trias = Delaunay(points)
colors = random_pallet(15)
for tria in points[trias.simplices]:
    tria_color = "#{:02x}{:02x}{:02x}ff".format(*get_color(np.mean(tria, axis=0)))
    d.polygon(
        list(np.reshape(tria, (6,))),
        fill=tria_color,
        outline=tria_color[:7]+"99"
    )
a = a.resize((1920, 1080), resample=Image.ANTIALIAS)
a.save(f"{__file__}.png")